const creditCard = {
    cash: 10000,
    pin: '6996',
    toBlock: 3,
    status: 'active',
    answers: {
        blocked: 'Ваша карта заблокирована, обратитесь в банк для ее разблокировки',
        incorrectPinBlock: 'Неправильный пин-код! Вы исчерпали количество попыток. Ваша карта заблокирована, обратитесь в банк для ее разблокировки',
        incorrectPin: 'Неправильный пин-код! Попробуйте пожалуйста снова!',
        noMoney: 'К сожалению, на вашем счету недостаточно средств',
        success: 'Получите ваши - сумма, которую человек хочет снять - ',
    },
    getCash: function (pin, value) {
        if (this.status !== 'disabled') {
            if (pin === this.pin) {
                this.toBlock = 3;
                if (value <= this.cash) {
                    this.cash -= value;
                    return this.answers.success + value;
                } else {
                    return this.answers.noMoney;
                }
            } else {
                if (--this.toBlock > 0) {
                    return this.answers.incorrectPin;
                } else {
                    this.status = 'disabled';
                    return this.answers.incorrectPinBlock;
                }
            }
        } else {
            return this.answers.blocked;
        }
    }
};
const pin = document.querySelector('#pin-code');
const sum = document.querySelector('#money-sum');
const result = document.querySelector('#cash-request-result');
const submit = document.querySelector('#get-cash');
submit.addEventListener('click', () => {
    console.log('Click');
    result.innerText = creditCard.getCash(pin.value, sum.value);
});
