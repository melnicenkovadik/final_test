let dogsFavor = [{
    name: "Мария",
    lastName: "Салтыкова",
    age: 25
}, {
    name: "Осана",
    lastName: "Меньшинова",
    age: 20
}, {
    name: "Андрей",
    lastName: "Первозваный",
    age: 100
}, {
    name: "Василий",
    lastName: "Гофман",
    age: 40
}, {
    name: "Поручик",
    lastName: "Ржевский",
    age: "вечно молодой"
}];

let catsFavor = [{
    name: "Мария",
    lastName: "Розгозина",
    age: 22
}, {
    name: "Осана",
    lastName: "Меньшинова",
    age: 20
}, {
    name: "Андрей",
    lastName: "Первозваный",
    age: 100
}, {
    name: "Алексей",
    lastName: "Гофман",
    age: 40
}, {
    name: "Капитан",
    lastName: "Очевидность",
    age: "вечно молодой"
}];


function arrayFilter(firstArray, secondArray) {
    return firstArray.filter((firstElement) => !secondArray.some((secondElement => secondElement.lastName === firstElement.lastName && secondElement.name === firstElement.name)))
}

const peopleInHeaven = arrayFilter(dogsFavor, catsFavor);
console.log(peopleInHeaven);