const englishBreakfast = ["поджареный бекон", "колбаски", "яичница", "жареные грибы", "жареные помидоры", "фасоль", "хлеб с джемом", "кофе"];

/* в результате массив englishBreakfast должен быть равен:
englishBreakfast = ["поджареный бекон", "колбаски", "яичница", "жареные грибы"];

и должен появится еще массив dinner, со значениями:
dinner = ["жареные помидоры", "фасоль", "хлеб с джемом", "кофе"];
*/

let dinner = englishBreakfast.splice(4);
englishBreakfast.splice();

console.group('Result');
console.log('Breakfast', englishBreakfast);
console.log('Dinner', dinner);
console.groupEnd();
