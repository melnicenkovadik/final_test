const comment = document.querySelector('#comment');
const spamWord = document.querySelector('#spam-word');
const submit = document.querySelector('#send-comment');
const result = document.querySelector('#spam-check-result');
submit.addEventListener('click', () => {
    result.innerText = isSpam(comment.value, spamWord.value, 3)
});
function isSpam(comment, spamWord, count) {
    return comment.split(spamWord).length - 1 >= count;
}